/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
var partner_trees = [{
        //arancia navelina
        "fruit_id": 682,
        "number": 160,
        "co2": "11,2",
        "location_country": "Corigliano-Rossano, Calabria",
        "location_province": "Piana di Sibari, Corigliano-Rossano (CS)"
    },
    {
        //mela fuji
        "fruit_id": 681,
        "number": 100,
        "co2": "4,1",
        "location_country": "Clès, Trentino",
        "location_province": "Val di Non, Trento (TN)"
    },
    {
        //clementina calabrese
        "fruit_id": 683,
        "number": 170,
        "co2": "11",
        "location_country": "Corigliano-Rossano, Calabria",
        "location_province": "Piana di Sibari, Corigliano-Rossano (CS)"
    },
    {
        //pera williams
        "fruit_id": 11636,
        "number": 120,
        "co2": "6,6",
        "location_country": "Lagnasco, Piemonte",
        "location_province": "Lagnasco, Cuneo (CN)"
    },
    {
        //pesca gialla
        "fruit_id": 11701,
        "number": 100,
        "co2": "8,9",
        "location_country": "Lagnasco, Piemonte",
        "location_province": "Lagnasco, Cuneo (CN)"
    },
    {
        //albicocca pellecchiella
        "fruit_id": 5047,
        "number": 90,
        "co2": "6,3",
        "location_country": "Piana del Sele, Campania",
        "location_province": "Piana del Sele, Salerno (SA)"
    },
    {
        //susina nera
        "fruit_id": 17811,
        "number": 97,
        "co2": "6,9",
        "location_country": "Faenza, Emilia Romagna",
        "location_province": "Faenza, Ravenna (RA)"
    },
    {
        //nettarina di faenza
        "fruit_id": 17812,
        "number": 60,
        "co2": "5,3",
        "location_country": "Faenza, Emilia Romagna",
        "location_province": "Faenza, Ravenna (RA)"
    },
    {
        //mandorla di noto
        "fruit_id": 14646,
        "number": 80,
        "co2": "2,5",
        "location_country": "Noto, Sicilia",
        "location_province": "Noto, Siracusa (SR)"
    },
    {
        //mela gala
        "fruit_id": 11526,
        "number": 90,
        "co2": "4,2",
        "location_country": "Lagnasco, Piemonte",
        "location_province": "Cuneo (CN)"
    },
    {
        //limone zagara bianca
        "fruit_id": 33671,
        "number": 50,
        "co2": "3,5",
        "location_country": "Rocca di Capri Leone, Sicilia",
        "location_province": "Rocca di Capri Leone (ME)"
    },
    {
        //limone di sicilia
        "fruit_id": 684,
        "number": 20,
        "co2": "1,4",
        "location_country": "Marsala, Sicilia",
        "location_province": "Marsala (TP)"
    },
    {
        //albicocca rossa
        "fruit_id": 32881,
        "number": 1,
        "co2": "1",
        "location_country": "San Ferdinando, Puglia",
        "location_province": "San Ferdinando di Puglia, Barletta-Andria-Trani (BT)"
    },
    {
        //arancia valencia
        "fruit_id": 14648,
        "number": 5,
        "co2": "0,3",
        "location_country": "Noto, Sicilia",
        "location_province": "Noto (SR)"
    },
    {
        //avocado fuerte
        "fruit_id": 33670,
        "number": 1,
        "co2": "1",
        "location_country": "Rocca di Capri Leone, Sicilia",
        "location_province": "Rocca di Capri Leone (ME)"
    },
    {
        //avocado hass
        "fruit_id": 33669,
        "number": 1,
        "co2": "1",
        "location_country": "Rocca di Capri Leone, Sicilia",
        "location_province": "Rocca di Capri Leone (ME)"
    },
    {
        //bergamotto di calabria
        "fruit_id": 17718,
        "number": 9,
        "co2": "0,6",
        "location_country": "Ardore, Calabria",
        "location_province": "Ardore (RC)"
    },
    {
        //carruba
        "fruit_id": 14647,
        "number": 1,
        "co2": "1",
        "location_country": "Noto, Sicilia",
        "location_province": "Noto (SR)"
    },
    {
        //castagna del prete
        "fruit_id": 37212,
        "number": 1,
        "co2": "1",
        "location_country": "Montella, Campania",
        "location_province": "Montella (AV)"
    },
    {
        //castagna palummina
        "fruit_id": 37211,
        "number": 1,
        "co2": "1",
        "location_country": "Montella, Campania",
        "location_province": "Montella (AV)"
    },
    {
        //cedro diamante
        "fruit_id": 33672,
        "number": 1,
        "co2": "1",
        "location_country": "Rocca di Capri Leone, Sicilia",
        "location_province": "Rocca di Capri Leone (ME)"
    },
    {
        //ciliegia ferrovia
        "fruit_id": 31973,
        "number": 1,
        "co2": "1",
        "location_country": "Molfetta, Puglia",
        "location_province": "Molfetta (BA)"
    },
    {
        //fico d'india bianco
        "fruit_id": 12274,
        "number": 1,
        "co2": "1",
        "location_country": "Barrafranca, Sicilia",
        "location_province": "Barrafranca, Enna (EN)"
    },
    {
        //fico d'india giallo
        "fruit_id": 12370,
        "number": 1,
        "co2": "1",
        "location_country": "Barrafranca, Sicilia",
        "location_province": "Barrafranca, Enna (EN)"
    },
    {
        //fico d'india rosso
        "fruit_id": 12368,
        "number": 1,
        "co2": "1",
        "location_country": "Barrafranca, Sicilia",
        "location_province": "Barrafranca, Enna (EN)"
    },
    {
        //fico d'india mix
        "fruit_id": 12372,
        "number": 1,
        "co2": "0,085",
        "location_country": "Barrafranca, Sicilia",
        "location_province": "Barrafranca, Enna (EN)"
    },
    {
        //kumquat
        "fruit_id": 33673,
        "number": 20,
        "co2": "1,3",
        "location_country": "Rocca di Capri Leone, Sicilia",
        "location_province": "Rocca di Capri Leone (ME)"
    },
    {
        //lime
        "fruit_id": 17719,
        "number": 1,
        "co2": "1",
        "location_country": "Ardore, Calabria",
        "location_province": "Ardore (RC)"
    },
    {
        //limone di calabria
        "fruit_id": 38775,
        "number": 5,
        "co2": "0,3",
        "location_country": "Ardore, Calabria",
        "location_province": "Ardore (RC)"
    },
    {
        //limone verdello
        "fruit_id": 38777,
        "number": 1,
        "co2": "1",
        "location_country": "Ardore, Calabria",
        "location_province": "Ardore (RC)"
    },
    {
        //mango keitt
        "fruit_id": 33668,
        "number": 1,
        "co2": "1",
        "location_country": "Rocca di Capri Leone, Sicilia",
        "location_province": "Rocca di Capri Leone (ME)"
    },
    {
        //mango tommy atkins
        "fruit_id": 33667,
        "number": 1,
        "co2": "1",
        "location_country": "Rocca di Capri Leone, Sicilia",
        "location_province": "Rocca di Capri Leone (ME)"
    },
    {
        //mela golden delicious
        "fruit_id": 679,
        "number": 1,
        "co2": "1",
        "location_country": "Clès, Trentino",
        "location_province": "Val di Non, Trento (TN)"
    },
    {
        //mela renetta del canada
        "fruit_id": 632,
        "number": 4,
        "co2": "0,2",
        "location_country": "Clès, Trentino",
        "location_province": "Val di Non, Trento (TN)"
    },
    {
        //melograno akko
        "fruit_id": 33357,
        "number": 1,
        "co2": "1",
        "location_country": "Alcamo, Sicilia",
        "location_province": "Alcamo (TP)"
    },
    {
        //melograno wonderful
        "fruit_id": 33356,
        "number": 1,
        "co2": "1",
        "location_country": "Alcamo, Sicilia",
        "location_province": "Alcamo (TP)"
    },
    {
        //olivo coratina
        "fruit_id": 28708,
        "number": 1,
        "co2": "1",
        "location_country": "Minervino, Puglia",
        "location_province": "Minervino Murge, (BT)"
    },
    {
        //olivo secolare dop bruzio
        "fruit_id": 685,
        "number": 1,
        "co2": "1",
        "location_country": "Colline pre-Silane, Calabria",
        "location_province": "Colline pre-Silane, Corigliano-Rossano (CS)"
    },
    {
        //pera buona luisa
        "fruit_id": 678,
        "number": 1,
        "co2": "1",
        "location_country": "Clès, Trentino",
        "location_province": "Val di Non, Trento (TN)"
    },
    {
        //uva vittoria
        "fruit_id": 32926,
        "number": 1,
        "co2": "1",
        "location_country": "San Ferdinando, Puglia",
        "location_province": "San Ferdinando di Puglia, Barletta-Andria-Trani (BT)"
    },
    {
        //arancia tarocco
        "fruit_id": 60416,
        "number": 1,
        "co2": "1",
        "location_country": "Grammichele, Sicilia",
        "location_province": "Grammichele, Catania (CT)"
    },
    {
        //caco loto di romanga
        "fruit_id": 61341,
        "number": 1,
        "co2": "1",
        "location_country": "Imola, Emilia-Romagna",
        "location_province": "Imola, Bologna (BO)"
    },
    {
        //olivo caninese
        "fruit_id": 61147,
        "number": 1,
        "co2": "1",
        "location_country": "Farnese, Lazio",
        "location_province": "Farnese, Viterbo (VT)"
    },
    {
        //mela grigia di torriana
        "fruit_id": 60809,
        "number": 1,
        "co2": "1",
        "location_country": "Barge, Piemonte",
        "location_province": "Barge, Cuneo (CN)"
    },
    {
        //mela crimson
        "fruit_id": 60808,
        "number": 1,
        "co2": "1",
        "location_country": "Barge, Piemonte",
        "location_province": "Barge, Cuneo (CN)"
    },
    {
        //susina ramassin
        "fruit_id": 60807,
        "number": 1,
        "co2": "1",
        "location_country": "Barge, Piemonte",
        "location_province": "Barge, Cuneo (CN)"
    },
    {
        //germogli di bambù
        "fruit_id": 60703,
        "number": 1,
        "co2": "1",
        "location_country": "Castel Rozzone, Lombardia",
        "location_province": "Castel Rozzone, Bergamo (BG)"
    },
    {
        //kiwi boerica
        "fruit_id": 61443,
        "number": 1,
        "co2": "1",
        "location_country": "Taurianova, Calabria",
        "location_province": "Taurianova, Reggio Calabria (RC)"
    },
    {
        //arancia vaniglia
        "fruit_id": 61686,
        "number": 1,
        "co2": "1",
        "location_country": "Burgio, Sicilia",
        "location_province": "Burgio, Agrigento (AG)"
    },
    {
        //arancia washington navel
        "fruit_id": 61687,
        "number": 1,
        "co2": "1",
        "location_country": "Burgio, Sicilia",
        "location_province": "Burgio, Agrigento (AG)"
    },
    {
        //mirtillo duke
        "fruit_id": 61900,
        "number": 1,
        "co2": "1",
        "location_country": "Barge, Piemonte",
        "location_province": "Barge, Cuneo (CN)"
    },
    {
        //nocciola nebrodi
        "fruit_id": 62033,
        "number": 1,
        "co2": "1",
        "location_country": "Brolo, Sicilia",
        "location_province": "Brolo, Messina (ME)"
    },
    {
        //kiwi hayward
        "fruit_id": 62374,
        "number": 1,
        "co2": "1",
        "location_country": "Teverola, Campania",
        "location_province": "Teverola, Caserta (CE)"
    },
    {
        //susina sorriso di primavera
        "fruit_id":  63670,
        "number": 1,
        "co2": "1",
        "location_country": "Piana del Sele, Campania",
        "location_province": "Piana del Sele, Salerno (SA)"
    },
    {
        //mela granny smith
        "fruit_id":  63819,
        "number": 1,
        "co2": "1",
        "location_country": "Clès, Trentino",
        "location_province": "Val di Non, Trento (TN)"
    },
    {
        //mandorla sgusciata
        "fruit_id":  63770,
        "number": 1,
        "co2": "1",
        "location_country": "Minervino, Puglia",
        "location_province": "Minervino, Barletta-Andria-Trani (BT)"
    },
    {
        //mela stark
        "fruit_id":  63726,
        "number": 1,
        "co2": "1",
        "location_country": "Clès, Trentino",
        "location_province": "Val di Non, Trento (TN)"
    },
    {
        //pera coscia
        "fruit_id": 69905,
        "number": 1,
        "co2": "0,07",
        "location_country": "Cesarò, Sicilia",
        "location_province": "Cesarò, Messina (ME)"
    },
    {
        //uva mystery bianca
        "fruit_id": 70020,
        "number": 1,
        "co2": "0,07",
        "location_country": "Ginosa, Puglia",
        "location_province": "Ginosa, Taranto (TA)"
    },
    {
        //uva mystery bianca
        "fruit_id": 70020,
        "number": 1,
        "co2": "0,07",
        "location_country": "Ginosa, Puglia",
        "location_province": "Ginosa, Taranto (TA)"
    },
    {
        //uva crimson rossa
        "fruit_id": 70009,
        "number": 1,
        "co2": "0,07",
        "location_country": "Ginosa, Puglia",
        "location_province": "Ginosa, Taranto (TA)"
    },
    {
        //uva midnight nera
        "fruit_id": 70019,
        "number": 1,
        "co2": "0,07",
        "location_country": "Ginosa, Puglia",
        "location_province": "Ginosa, Taranto (TA)"
    },
    {
        //olio igp toscano
        "fruit_id":  70580,
        "number": 1,
        "co2": "0,07",
        "location_country": "Grosseto, Toscana",
        "location_province": "Roccastrada, Grosseto (GR)"
    },
    {
        //noce in guscio
        "fruit_id": 70519,
        "number": 1,
        "co2": "0,07",
        "location_country": "Campobasso, Molise",
        "location_province": "Campobasso (CB)"
    },
    {
        //pera abate
        "fruit_id": 71503,
        "number": 1,
        "co2": "0,07",
        "location_country": "Lagnasco, Piemonte",
        "location_province": "Cuneo (CN)"
    },
    {
        //pera williams sacchetto
        "fruit_id": 71557,
        "number": 1,
        "co2": "0,07",
        "location_country": "Lagnasco, Piemonte",
        "location_province": "Cuneo (CN)"
    }
    /*
    {
        //tree name
        "fruit_id": xxxxx,
        "number": integer,
        "co2": "string",
        "location_country": "città, regione",
        "location_province": "città, provinca (PROVINCIA)"
    }
    */
];

/*
************** CO2 MANCANTE

//pera williams sacchetto

//pera abate

//noce in guscio

//olio igp toscano

//pera coscia

//uva midnight nera

//uva crimson rossa

//uva mystery bianca

//mela stark

//mandorla sgusciata

//susina sorriso di primavera

//mela granny smith

//kiwi hayward

//nocciola nebrodi

//mirtillo duke

//arancia washington navel

//arancia vaniglia

//germogli di bambù

//susina ramassin

//mela crimson

//mela grigia di torriana

//olivo caninese

//caco loto di romanga

//arancia tarocco

//uva vittoria

//olivo secolare dop bruzio

//olivo coratina

//melograno wonderful

//melograno akko

//limone verdello

//mango keitt

//mango tommy atkins

//mela golden delicious
    
//lime

//carruba

//castagna del prete

//castagna palummina

//cedro diamante

//ciliegia ferrovia

//fico d'india bianco

//fico d'india giallo

//fico d'india rosso

//fico d'india mix
     
//albicocca rossa
      
//avocado fuerte

//avocado hass

//kiwi boerica
*/