/****** CO2******/
//  newCO2 = (newNumber * co2) / number
/*************/
var partner_trees = [
    {
    //arancia navelina
        "fruit_id": 682,
        "number": 20,
        "co2": "1,4",
        "location_country": "Corigliano-Rossano, Calabria",
        "location_province": "Piana di Sibari, Corigliano-Rossano (CS)"
    },
    {
        //clementina calabrese
        "fruit_id": 683,
        "number": 20,
        "co2": "1,3",
        "location_country": "Corigliano-Rossano, Calabria",
        "location_province": "Piana di Sibari, Corigliano-Rossano (CS)"
    },
    {
        //pera williams
        "fruit_id": 11636,
        "number": 20,
        "co2": "1,1",
        "location_country": "Lagnasco, Piemonte",
        "location_province": "Lagnasco, Cuneo (CN)"
    },
    {
        //mela gala
        "fruit_id": 11526,
        "number": 20,
        "co2": "0,9",
        "location_country": "Lagnasco, Piemonte",
        "location_province": "Cuneo (CN)"
    },
    {
        //limone di sicilia
        "fruit_id": 684,
        "number": 20,
        "co2": "1,4",
        "location_country": "Marsala, Sicilia",
        "location_province": "Marsala (TP)"
    }
];