/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [

    {
        "fruit_id": 682,
        "number": 6,
        "co2": "0,4",//0,42
        "location_country": "Corigliano-Rossano, Calabria",
        "location_province": "Piana di Sibari, Corigliano-Rossano (CS)"
    },
    {
        "fruit_id": 681,
        "number": 8,
        "co2": "0,3", //0,32
        "location_country": "Clès, Trentino",
        "location_province": "Val di Non, Clès (TN)"
    },
    {
        "fruit_id": 683,
        "number": 8,
        "co2": "0,5", //0,48
        "location_country": "Corigliano-Rossano, Calabria",
        "location_province": "Piana di Sibari, Corigliano-Rossano (CS)"
    },
    {
        "fruit_id": 11526,
        "number": 7,
        "co2": "0,4", //0,35
        "location_country": "Lagnasco, Piemonte",
        "location_province": "Cuneo (CN)"
    },
    {
        "fruit_id": 38775,
        "number": 8,
        "co2": "0,5", //0,48
        "location_country": "Ardore, Calabria",
        "location_province": "Ardore (RC)"
    },
    {
        "fruit_id": 60416,
        "number": 6,
        "co2": "0,4", //0,42
        "location_country": "Grammichele, Sicilia",
        "location_province": "Grammichele, Catania (CT)"
    },
    {
        "fruit_id": 71557,
        "number": 2,
        "co2": "0,2", //0,14
        "location_country": "Lagnasco, Piemonte",
        "location_province": "Cuneo (CN)"
    }

]