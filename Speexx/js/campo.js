/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
var partner_trees = [
    {
    //arancia navelina
        "fruit_id": 682,
        "number": 9,
        "co2": "0,6",
        "location_country": "Corigliano-Rossano, Calabria",
        "location_province": "Piana di Sibari, Corigliano-Rossano (CS)"
    },
    {
        //arancia valencia
        "fruit_id": 14648,
        "number": 9,
        "co2": "0,5",
        "location_country": "Noto, Sicilia",
        "location_province": "Noto (SR)"
    },
    {
        //bergamotto di calabria
        "fruit_id": 17718,
        "number": 5,
        "co2": "0,3",
        "location_country": "Ardore, Calabria",
        "location_province": "Ardore (RC)"
    },
    {
        //mela gala
        "fruit_id": 11526,
        "number": 3,
        "co2": "0,1",
        "location_country": "Lagnasco, Piemonte",
        "location_province": "Cuneo (CN)"
    },
    {
        //pera williams
        "fruit_id": 11636,
        "number": 3,
        "co2": "0,2",
        "location_country": "Lagnasco, Piemonte",
        "location_province": "Lagnasco, Cuneo (CN)"
    },
    {
        //fico d'india mix
        "fruit_id": 12372,
        "number": 5,
        "co2": "0,3",
        "location_country": "Barrafranca, Sicilia",
        "location_province": "Barrafranca, Enna (EN)"
    }
];