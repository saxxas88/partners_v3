/****** CO2******/
//  newCO2 = (newNumber * co2) / number
/*************/
var partner_trees = [{
    //arancia navelina
        "fruit_id": 682,
        "number": 20,
        "co2": "1",
        "location_country": "Rossano, Calabria",
        "location_province": "Piana di Sibari, Rossano (CS)"
    },
    {
        //arancia valencia
        "fruit_id": 14648,
        "number": 20,
        "co2": "1,2",
        "location_country": "Noto, Sicilia",
        "location_province": "Noto (SR)"
    },
    {
        //clementina calabrese
        "fruit_id": 683,
        "number": 20,
        "co2": "1",
        "location_country": "Corigliano-Rossano, Calabria",
        "location_province": "Piana di Sibari, Corigliano-Rossano (CS)"
    },
    {
        //kumquat
        "fruit_id": 33673,
        "number": 20,
        "co2": "1,3",
        "location_country": "Rocca di Capri Leone, Sicilia",
        "location_province": "Rocca di Capri Leone (ME)"
    },
    {
        //limone zagara bianca
        "fruit_id": 33671,
        "number": 20,
        "co2": "1,4",
        "location_country": "Rocca di Capri Leone, Sicilia",
        "location_province": "Rocca di Capri Leone (ME)"
    }
];