/****** CO2******/
//  newCO2 = (newNumber * co2) / number
/*************/
var partner_trees = [{
    //arancia navelina
        "fruit_id": 682,
        "number": 5,
        "co2": "0,3",
        "location_country": "Rossano, Calabria",
        "location_province": "Piana di Sibari, Rossano (CS)"
    },
    {
        //clementica calabrese
        "fruit_id": 683,
        "number": 5,
        "co2": "0,3",
        "location_country": "Corigliano-Rossano, Calabria",
        "location_province": "Piana di Sibari, Corigliano-Rossano (CS)"
    },
    {
        //mela gala
        "fruit_id": 11526,
        "number": 5,
        "co2": "0,2",
        "location_country": "Lagnasco, Piemonte",
        "location_province": "Cuneo (CN)"
    },
    {
        //pera williams
        "fruit_id": 11636,
        "number": 5,
        "co2": "0,3",
        "location_country": "Lagnasco, Piemonte",
        "location_province": "Lagnasco, Cuneo (CN)"
    },
    {
        //arancia valencia
        "fruit_id": 14648,
        "number": 5,
        "co2": "0,3",
        "location_country": "Noto, Sicilia",
        "location_province": "Noto (SR)"
    },
    {
        //limone di calabria
        "fruit_id": 38775,
        "number": 5,
        "co2": "0,3",
        "location_country": "Ardore, Calabria",
        "location_province": "Ardore (RC)"
    },
];