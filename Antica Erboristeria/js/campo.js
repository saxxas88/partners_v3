/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
var partner_trees = [{
    //arancia navelina
        "fruit_id": 682,
        "number": 80,
        "co2": "4,4",
        "location_country": "Rossano, Calabria",
        "location_province": "Piana di Sibari, Rossano (CS)"
    },
    {
        //arancia valencia
        "fruit_id": 14648,
        "number": 80,
        "co2": "4,8",
        "location_country": "Noto, Sicilia",
        "location_province": "Noto (SR)"
    },
    {
        //pesca gialla
        "fruit_id": 11701,
        "number": 60,
        "co2": "4,8",
        "location_country": "Lagnasco, Piemonte",
        "location_province": "Lagnasco, Cuneo (CN)"
    },
    {
        //mela gala
        "fruit_id": 11526,
        "number": 80,
        "co2": "3,2",
        "location_country": "Lagnasco, Piemonte",
        "location_province": "Cuneo (CN)"
    },
    {
        //pera williams
        "fruit_id": 11636,
        "number": 80,
        "co2": "4,4",
        "location_country": "Lagnasco, Piemonte",
        "location_province": "Lagnasco, Cuneo (CN)"
    },
    {
        //limone zagara bianca
        "fruit_id": 33671,
        "number": 80,
        "co2": "5,6",
        "location_country": "Rocca di Capri Leone, Sicilia",
        "location_province": "Rocca di Capri Leone (ME)"
    },
    {
        //fico d'india mix
        "fruit_id": 12372,
        "number": 80,
        "co2": "4",
        "location_country": "Barrafranca, Sicilia",
        "location_province": "Barrafranca, Enna (EN)"
    }
];