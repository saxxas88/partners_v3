CO2 totali: 31.200 Kg

Agricoltori supportati: 5

Codice Sconto: antica10

Logo URL: https://res.cloudinary.com/biorfarm/image/upload/v1575904339/static/antica-erboristeria/logo.png

Slogan: "Da trentacinque anni i ricercatori Antica Erboristeria si dedicano allo studio delle proprietà specifiche delle erbe e degli ingredienti naturali capaci di donare salute, bellezza e protezione. Riscopri la semplicità della natura a casa e segui la coltivazione del tuo albero da frutto Bio."