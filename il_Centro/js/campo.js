/****** CO2******/
//  newCO2 = (newNumber * co2) / number
/*************/
var partner_trees = [

    {
        //albicocca pellecchiella
        "fruit_id": 5047,
        "number": 4,
        "co2": "0,3",
        "location_country": "Piana del Sele, Eboli",
        "location_province": "Piana del Sele, Eboli (SA)"
    },
    {
        //arancia navelina
            "fruit_id": 682,
            "number": 2,
            "co2": "0,1",
            "location_country": "Rossano, Calabria",
            "location_province": "Piana di Sibari, Rossano (CS)"
        },
    {
        //arancia valencia
        "fruit_id": 14648,
        "number": 3,
        "co2": "0,2",
        "location_country": "Noto, Sicilia",
        "location_province": "Noto (SR)"
    },
    {
        //clementica calabrese
        "fruit_id": 683,
        "number": 3,
        "co2": "0,2",
        "location_country": "Corigliano-Rossano, Calabria",
        "location_province": "Piana di Sibari, Corigliano-Rossano (CS)"
    },
{
    //mela renetta del canada
    "fruit_id": 632,
    "number": 4,
    "co2": "0,2",
    "location_country": "Clès, Trentino",
    "location_province": "Val di Non, Clès (TN)"
},
{
    //nettarina di faenza
    "fruit_id": 17812,
    "number": 1,
    "co2": "0,1",
    "location_country": "Faenza, Emilia Romagna",
    "location_province": "Faenza (RA)"
},

{
    //pera williams
    "fruit_id": 11636,
    "number": 3,
    "co2": "0,2",
    "location_country": "Lagnasco, Piemonte",
    "location_province": "Lagnasco, Cuneo (CN)"
},

    
    {
        //pesca gialla
        "fruit_id": 11701,
        "number": 1,
        "co2": "0,1",
        "location_country": "Lagnasco, Piemonte",
        "location_province": "Lagnasco, Cuneo (CN)"
    }
  
];