/****** CO2******/
//  newCO2 = (newNumber * co2) / number
/*************/
var partner_trees = [{
    "fruit_id": 682,
    "number": 50,
    "co2": "3,5",
    "location_country": "Rossano, Calabria",
    "location_province": "Piana di Sibari, Rossano (CS)"
},
{
    "fruit_id": 683,
    "number": 50,
    "co2": "3,3",
    "location_country": "Rossano, Calabria",
    "location_province": "Piana di Sibari, Rossano (CS)"
},
{
    "fruit_id": 11636,
    "number": 50,
    "co2": "2,8",
    "location_country": "Lagnasco, Piemonte",
    "location_province": "Lagnasco, Cuneo (CN)"
},
{
    "fruit_id": 11526,
    "number": 50,
    "co2": "2,3",
    "location_country": "Lagnasco, Piemonte",
    "location_province": "Cuneo (CN)"
},
{
    "fruit_id": 33671,
    "number": 50,
    "co2": "3,5",
    "location_country": "Rocca di Capri Leone",
    "location_province": "Rocca di Capri Leone (ME)"
}
];