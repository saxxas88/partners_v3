/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
var partner_trees = [{
        //albicocca pellecchiella
        "fruit_id": 5047,
        "number": 5,
        "co2": "0,4",
        "location_country": "Piana del Sele, Eboli",
        "location_province": "Piana del Sele, Eboli (SA)"
    },
    {
        //albicocca rossa
        "fruit_id": 32881,
        "number": 5,
        "co2": "0,4",
        "location_country": "San Ferdinando di Puglia",
        "location_province": "San Ferdinando di Puglia (BT)"
    },
    {
        //arancia navelina
        "fruit_id": 682,
        "number": 5,
        "co2": "0,4",
        "location_country": "Corigliano-Rossano, Calabria",
        "location_province": "Piana di Sibari, Corigliano-Rossano (CS)"
    },
    {
        //arancia valencia
        "fruit_id": 14648,
        "number": 5,
        "co2": "0,3",
        "location_country": "Noto, Sicilia",
        "location_province": "Noto (SR)"
    },
    {
        //pera williams
        "fruit_id": 11636,
        "number": 5,
        "co2": "0,3",
        "location_country": "Lagnasco, Piemonte",
        "location_province": "Lagnasco, Cuneo (CN)"
    },
    {
        //pesca gialla
        "fruit_id": 11701,
        "number": 5,
        "co2": "0,5",
        "location_country": "Lagnasco, Piemonte",
        "location_province": "Lagnasco, Cuneo (CN)"
    },
    {
        //susina nera
        "fruit_id": 17811,
        "number": 5,
        "co2": "0,4",
        "location_country": "Faenza, Emilia Romagna",
        "location_province": "Faenza (RA)"
    },
    {
        //clementina calabrese
        "fruit_id": 683,
        "number": 5,
        "co2": "0,3",
        "location_country": "Corigliano-Rossano, Calabria",
        "location_province": "Piana di Sibari, Corigliano-Rossano (CS)"
    },
    {
        //kumquat
        "fruit_id": 33673,
        "number": 1,
        "co2": "0,1",
        "location_country": "Rocca di Capri Leone, Sicilia",
        "location_province": "Rocca di Capri Leone (ME)"
    },
    {
        //mandorla di noto
        "fruit_id": 14646,
        "number": 1,
        "co2": "0,1",
        "location_country": "Noto, Sicilia",
        "location_province": "Noto (SR)"
    },
    {
        //mela fuji
        "fruit_id": 681,
        "number": 2,
        "co2": "0,1",
        "location_country": "Clès, Trentino",
        "location_province": "Val di Non, Clès (TN)"
    },
    {
        //mela gala
        "fruit_id": 11526,
        "number": 5,
        "co2": "0,2",
        "location_country": "Lagnasco, Piemonte",
        "location_province": "Cuneo (CN)"
    },
    {
        //nettarina di faenza
        "fruit_id": 17812,
        "number": 3,
        "co2": "0,3",
        "location_country": "Faenza, Emilia Romagna",
        "location_province": "Faenza (RA)"
    },
    {
        //mela golden delicious
        "fruit_id": 679,
        "number": 3,
        "co2": "0,3",
        "location_country": "Clès, Trentino",
        "location_province": "Val di Non, Clès (TN)"
    },
    {
        //avocado hass
        "fruit_id": 33669,
        "number": 1,
        "co2": "0,1",
        "location_country": "Rocca di Capri Leone, Sicilia",
        "location_province": "Rocca di Capri Leone (ME)"
    },
    {
        //castagna del prete
        "fruit_id": 37212,
        "number": 1,
        "co2": "0,1",
        "location_country": "Montella, Campania",
        "location_province": "Montella (AV)"
    },
    {
        //ciliegia ferrovia
        "fruit_id": 31973,
        "number": 1,
        "co2": "0,1",
        "location_country": "Molfetta, Puglia",
        "location_province": "Molfetta (BA)"
    },

    {
        //fico d'india mix
        "fruit_id": 12372,
        "number": 3,
        "co2": "0,3",
        "location_country": "Barrafranca, Sicilia",
        "location_province": "Barrafranca, Enna (EN)"
    },
    {
        //pera buona luisa
        "fruit_id": 678,
        "number": 3,
        "co2": "0,2",
        "location_country": "Clès, Trentino",
        "location_province": "Val di Non, Clès (TN)"
    },
    {
        //uva vittoria
        "fruit_id": 32926,
        "number": 2,
        "co2": "0,2",
        "location_country": "San Ferdinando di Puglia",
        "location_province": "San Ferdinando di Puglia (BT)"
    },
    {
        //melograno wonderful
        "fruit_id": 33356,
        "number": 1,
        "co2": "0,1",
        "location_country": "Alcamo, Sicilia",
        "location_province": "Alcamo (TP)"
    },
    {
        //arancia tarocco
        "fruit_id": 60416,
        "number": 5,
        "co2": "0,4",
        "location_country": "Grammichele, Sicilia",
        "location_province": "Grammichele, Catania (CT)"
    },
    {
        //caco loto di romanga
        "fruit_id": 61341,
        "number": 2,
        "co2": "0,3",
        "location_country": "Imola, Emilia-Romagna",
        "location_province": "Imola (BO)"
    }
  
];