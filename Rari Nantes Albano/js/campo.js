/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
var partner_trees = [
    {
        //avocado fuerte
        "fruit_id": 33670,
        "number": 1,
        "co2": "0,1",
        "location_country": "Rocca di Capri Leone, Sicilia",
        "location_province": "Rocca di Capri Leone (ME)"
    },
    {
        //kumquat
        "fruit_id": 33673,
        "number": 1,
        "co2": "0,1",
        "location_country": "Rocca di Capri Leone, Sicilia",
        "location_province": "Rocca di Capri Leone (ME)"
    },
    {
        //limone di calabria
        "fruit_id": 38775,
        "number": 1,
        "co2": "0,1",
        "location_country": "Ardore, Calabria",
        "location_province": "Ardore (RC)"
    },
    {
        //arancia navelina
        "fruit_id": 682,
        "number": 1,
        "co2": "0,1",
        "location_country": "Corigliano-Rossano, Calabria",
        "location_province": "Piana di Sibari, Corigliano-Rossano (CS)"
    },
    {
        //arancia valencia
        "fruit_id": 14648,
        "number": 1,
        "co2": "0,1",
        "location_country": "Noto, Sicilia",
        "location_province": "Noto (SR)"
    },
    {
        //albicocca rossa
        "fruit_id": 32881,
        "number": 1,
        "co2": "0,1",
        "location_country": "San Ferdinando di Puglia, Puglia",
        "location_province": "San Ferdinando di Puglia (BT)"
    },
    {
        //albicocca pellecchiella
        "fruit_id": 5047,
        "number": 1,
        "co2": "0,1",
        "location_country": "Piana del Sele, Eboli",
        "location_province": "Piana del Sele, Eboli (SA)"
    },
    {
        //ciliegia ferrovia
        "fruit_id": 31973,
        "number": 1,
        "co2": "0,1",
        "location_country": "Molfetta, Puglia",
        "location_province": "Molfetta (BA)"
    },
    {
        //susina nera
        "fruit_id": 17811,
        "number": 1,
        "co2": "0,1",
        "location_country": "Faenza, Emilia Romagna",
        "location_province": "Faenza (RA)"
    },
    {
        //nettarina di faenza
        "fruit_id": 17812,
        "number": 1,
        "co2": "0,1",
        "location_country": "Faenza, Emilia Romagna",
        "location_province": "Faenza (RA)"
    },
    {
        //uva vittoria
        "fruit_id": 32926,
        "number": 1,
        "co2": "0,1",
        "location_country": "San Ferdinando, Puglia",
        "location_province": "San Ferdinando di Puglia (BT)"
    },
    {
        //pesca gialla
        "fruit_id": 11701,
        "number": 1,
        "co2": "0,1",
        "location_country": "Lagnasco, Piemonte",
        "location_province": "Lagnasco, Cuneo (CN)"
    }
];