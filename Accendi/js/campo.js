/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees=[

  {
   "fruit_id": 682,
   "number": 80,
   "co2": "5,6",
   "location_country": "Corigliano-Rossano, Calabria",
   "location_province": "Piana di Sibari, Corigliano-Rossano (CS)"
 } , 
  {
   "fruit_id": 683,
   "number": 80,
   "co2": "4,8",
   "location_country": "Corigliano-Rossano, Calabria",
   "location_province": "Piana di Sibari, Corigliano-Rossano (CS)"
 } , 
  {
   "fruit_id": 11526,
   "number": 90,
   "co2": "4,5",
   "location_country": "Lagnasco, Piemonte",
   "location_province": "Cuneo (CN)"
 } , 
 {
  "fruit_id": 33671,
  "number": 50,
  "co2": "3,5",
  "location_country": "Rocca di Capri Leone, Sicilia",
  "location_province": "Rocca di Capri Leone (ME)"
} ,  
  {
   "fruit_id": 17718,
   "number": 40,
   "co2": "2,8",
   "location_country": "Ardore, Calabria",
   "location_province": "Ardore (RC)"
 } , 
  {
   "fruit_id": 12372,
   "number": 70,
   "co2": "6,0", //5,95
   "location_country": "Barrafranca, Sicilia",
   "location_province": "Barrafranca, Enna (EN)"
 } , 
  {
   "fruit_id": 33357,
   "number": 60,
   "co2": "4,2",
   "location_country": "Alcamo, Sicilia",
   "location_province": "Alcamo (TP)"
 } , 
  {
   "fruit_id": 61443,
   "number": 70,
   "co2": "4,9",
   "location_country": "Taurianova, Calabria",
   "location_province": "Taurianova, Reggio Calabria (RC)"
 } , 
  {
   "fruit_id": 71557,
   "number": 60,
   "co2": "4,2",
   "location_country": "Lagnasco, Piemonte",
   "location_province": "Cuneo (CN)"
 }  
 
 ]