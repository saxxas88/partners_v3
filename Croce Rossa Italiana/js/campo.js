/****** CO2******/
//  newCO2 = (newNumber * co2) / number
/*************/
var partner_trees = [
    {
    //arancia navelina
        "fruit_id": 682,
        "number": 3,
        "co2": "0,2",
        "location_country": "Corigliano-Rossano, Calabria",
        "location_province": "Piana di Sibari, Corigliano-Rossano (CS)"
    },
    {
        //arancia valencia
        "fruit_id": 14648,
        "number": 3,
        "co2": "0,2",
        "location_country": "Noto, Sicilia",
        "location_province": "Noto (SR)"
    },
    {
        //kumquat
        "fruit_id": 33673,
        "number": 3,
        "co2": "0,2",
        "location_country": "Rocca di Capri Leone, Sicilia",
        "location_province": "Rocca di Capri Leone (ME)"
    },
    {
        //avocado fuerte
        "fruit_id": 33670,
        "number": 3,
        "co2": "0,2",
        "location_country": "Rocca di Capri Leone, Sicilia",
        "location_province": "Rocca di Capri Leone (ME)"
    },
    {
        //limone di calabria
        "fruit_id": 38775,
        "number": 3,
        "co2": "0,2",
        "location_country": "Ardore, Calabria",
        "location_province": "Ardore (RC)"
    },
    {
        //cedro diamante
        "fruit_id": 33672,
        "number": 3,
        "co2": "0,2",
        "location_country": "Rocca di Capri Leone, Sicilia",
        "location_province": "Rocca di Capri Leone (ME)"
    },
    {
        //albicocca rossa
        "fruit_id": 32881,
        "number": 3,
        "co2": "0,2",
        "location_country": "San Ferdinando di Puglia",
        "location_province": "San Ferdinando di Puglia (BT)"
    },
    {
        //albicocca pellecchiella
        "fruit_id": 5047,
        "number": 3,
        "co2": "0,2",
        "location_country": "Piana del Sele, Eboli",
        "location_province": "Piana del Sele, Eboli (SA)"
    }
]