/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 5047,
    "name": "albicocca pellecchiella old",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477692/fruits/biorfarm-icon-albicocca-pellecchiella_tree.png",
    "number": 7,
    "co2_unit": 0.07,
    "farmer_name": "Famiglia Dell'Orto",
    "location_country": "Piana del Sele, Campania",
    "location_province": "Piana del Sele, Salerno (SA)",
    "selected": true,
    "co2": "0,49"
  },
  {
    "fruit_id": 682,
    "name": "arancia navelina",
    "icon": "https://res.cloudinary.com/biorfarm/image/upload/v1561625041/fruits/biorfarm-icon-arancia_navelina_fruit.png",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477692/fruits/biorfarm-icon-arancia_navelina_tree.png",
    "img_tree": "https://www.biorfarm.com/wp-content/uploads/2020/03/Arance-Navel-di-Sicilia.jpg",
    "number": 9,
    "co2_unit": 0.07,
    "farmer_name": "Paolo De Falco",
    "location_country": "Corigliano-Rossano, Calabria",
    "location_province": "Piana di Sibari, Corigliano-Rossano (CS)",
    "selected": true,
    "co2": "0,63"
  },
  {
    "fruit_id": 14648,
    "name": "arancia valencia",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477692/fruits/biorfarm-icon-arancia_valencia_tree.png",
    "number": 4,
    "co2_unit": 0.07,
    "farmer_name": "Famiglia Leone",
    "location_country": "Noto, Sicilia",
    "location_province": "Noto, Siracusa (SR)",
    "selected": true,
    "co2": "0,28"
  },
  {
    "fruit_id": 683,
    "name": "clementina calabrese",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-clementina_tree.png",
    "number": 17,
    "co2_unit": 0.06,
    "farmer_name": "Paolo De Falco",
    "location_country": "Corigliano-Rossano, Calabria",
    "location_province": "Piana di Sibari, Corigliano-Rossano (CS)",
    "selected": true,
    "co2": "1,02"
  },
  {
    "fruit_id": 681,
    "name": "mela fuji",
    "icon": "https://res.cloudinary.com/biorfarm/image/upload/v1561625042/fruits/biorfarm-icon-mela_fuji_fruit.png",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-mela_fuji_tree.png",
    "img_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1500289952/fruits/mela_fuji_biorfarm_frutta.jpg",
    "number": 4,
    "co2_unit": 0.04,
    "farmer_name": "Paolo Rossi",
    "location_country": "Clès, Trentino",
    "location_province": "Val di Non, Clès (TN)",
    "selected": true,
    "co2": "0,16"
  },
  {
    "fruit_id": 127329,
    "name": "mela golden delicious",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-mela_golden_tree.png",
    "number": 4,
    "co2_unit": 0.04,
    "farmer_name": "Mirko Bezzi",
    "location_country": "Urbana, Veneto",
    "location_province": "Urbana, Padova (PD)",
    "selected": true,
    "co2": "0,16"
  },
  {
    "fruit_id": 678,
    "name": "pera buona luisa",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-pera_buona_luisa_tree.png",
    "number": 2,
    "co2_unit": 0.046,
    "farmer_name": "Paolo Rossi",
    "location_country": "Clès, Trentino",
    "location_province": "Val di Non, Trento (TN)",
    "selected": true,
    "co2": "0,09"
  },
  {
    "fruit_id": 71557,
    "name": "pera williams",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-pera_williams_tree.png",
    "number": 1,
    "co2_unit": 0.046,
    "farmer_name": "Lorenzo Sacchetto",
    "location_country": "Lagnasco, Piemonte",
    "location_province": "Lagnasco, Cuneo (CN)",
    "selected": true,
    "co2": "0,05"
  },
  {
    "fruit_id": 78574,
    "name": "uva bianca moscato",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1599730568/fruits/biorfarm-icon-uva-bianca-moscato_tree.png",
    "number": 2,
    "co2_unit": 0.04,
    "farmer_name": "Fabio Proverbio",
    "location_country": "Treviglio, Lombardia",
    "location_province": "Treviglio, Bergamo (BG)",
    "selected": true,
    "co2": "0,08"
  }
]