/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
var partner_trees = [
    {
        "fruit_id": 683,
        "number": 30,
        "co2": "1,9",
        "location_country": "Corigliano-Rossano, Calabria",
        "location_province": "Piana di Sibari, Corigliano-Rossano (CS)"
    },
    {
        "fruit_id": 5047,
        "number": 20,
        "co2": "1,4",
        "location_country": "Piana del Sele, Eboli",
        "location_province": "Piana del Sele, Eboli (SA)"
    },
    {
        "fruit_id": 11701,
        "number": 25,
        "co2": "2,5",
        "location_country": "Lagnasco, Piemonte",
        "location_province": "Lagnasco, Cuneo (CN)"
    },
    {
        "fruit_id": 14646,
        "number": 10,
        "co2": "1",
        "location_country": "Noto, Sicilia",
        "location_province": "Noto (SR)"
    },
   
    {
        "fruit_id": 12372,
        "number": 25,
        "co2": "1,9",
        "location_country": "Barrafranca, Sicilia",
        "location_province": "Barrafranca, Enna (EN)"
    },
    
    {
        "fruit_id": 33357,
        "number": 20,
        "co2": "2",
        "location_country": "Alcamo, Sicilia",
        "location_province": "Alcamo (TP)"
    }
    
  
];