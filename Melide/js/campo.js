/****** CO2******/
//  newCO2 = (newNumber * co2) / number
/*************/
var partner_trees = [{
    //arancia navelina
        "fruit_id": 682,
        "number": 9,
        "co2": "0,5",
        "location_country": "Rossano, Calabria",
        "location_province": "Piana di Sibari, Rossano (CS)"
    },
    {
        //clementica calabrese
        "fruit_id": 683,
        "number": 9,
        "co2": "0,5",
        "location_country": "Corigliano-Rossano, Calabria",
        "location_province": "Piana di Sibari, Corigliano-Rossano (CS)"
    },
    {
        //pesca gialla
        "fruit_id": 11701,
        "number": 5,
        "co2": "0,4",
        "location_country": "Lagnasco, Piemonte",
        "location_province": "Lagnasco, Cuneo (CN)"
    },
    {
        //bergamotto di calabria
        "fruit_id": 17718,
        "number": 3,
        "co2": "0,2",
        "location_country": "Ardore, Calabria",
        "location_province": "Ardore (RC)"
    },
    {
        //mela gala
        "fruit_id": 11526,
        "number": 5,
        "co2": "0,2",
        "location_country": "Lagnasco, Piemonte",
        "location_province": "Cuneo (CN)"
    }
];