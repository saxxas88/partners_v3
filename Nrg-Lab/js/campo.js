/****** CO2******/
//  newCO2 = (newNumber * co2) / number
/*************/
var partner_trees = [{
    //arancia navelina
        "fruit_id": 682,
        "number": 1,
        "co2": "0,1",
        "location_country": "Rossano, Calabria",
        "location_province": "Piana di Sibari, Rossano (CS)"
    },
    {
        //clementica calabrese
        "fruit_id": 683,
        "number": 23,
        "co2": "1,3",
        "location_country": "Corigliano-Rossano, Calabria",
        "location_province": "Piana di Sibari, Corigliano-Rossano (CS)"
    },
    {
        //arancia valencia
        "fruit_id": 14648,
        "number": 1,
        "co2": "0,1",
        "location_country": "Noto, Sicilia",
        "location_province": "Noto (SR)"
    }
];