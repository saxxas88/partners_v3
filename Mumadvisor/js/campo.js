/****** CO2******/
//  newCO2 = (newNumber * co2) / number
/*************/
var partner_trees = [
    {
        //clementina calabrese
        "fruit_id": 683,
        "number": 1,
        "co2": "0,1",
        "location_country": "Corigliano-Rossano, Calabria",
        "location_province": "Piana di Sibari, Corigliano-Rossano (CS)"
    },
    {
        //mela gala
        "fruit_id": 11526,
        "number": 1,
        "co2": "0,1",
        "location_country": "Lagnasco, Piemonte",
        "location_province": "Cuneo (CN)"
    },
    {
        //limone zagara bianca
        "fruit_id": 33671,
        "number": 1,
        "co2": "0,1",
        "location_country": "Rocca di Capri Leone, Sicilia",
        "location_province": "Rocca di Capri Leone (ME)"
    },
    {
        //arancia tarocco
        "fruit_id": 60416,
        "number": 1,
        "co2": "0,1",
        "location_country": "Grammichele, Sicilia",
        "location_province": "Grammichele, Catania (CT)"
    },
];